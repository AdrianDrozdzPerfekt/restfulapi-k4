<?php

use Faker\Generator as Faker;
use App\User;
use App\Buyer;
use App\Seller;
use App\Product;
use App\Category;
use App\Transaction;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
//USER
$factory->define(User::class, function (Faker $faker) {

	static $verified = $faker->randomElement([User::VERIFIED_USER, User::UNVERIFIED_USER]);
	static $admin = $faker->randomElement([User::ADMIN_USER, User::REGULAR_USER]);

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
        'verified' => $verified,
        'verification_token' => $verified == User::VERIFIED_USER ? null : User::generateVerificationCode(),
        'admin' => $verified == User::VERIFIED_USER ? $admin : User::REGULAR_USER,
    ];
});

//CATEGORY
$factory->define(Category::class, function (Faker $faker) {
	return [
		'name' => $faker->word,
		'description' => $faker->paragraph(1),
	]
});

//PRODUCT
$factory->define(Product::class, function (Faker $faker) {
	return [
		'name' => $faker->word,
		'description' => $faker->paragraph(1),
		'quantity' => $faker->numberBetween(1, 10),
		'status' => $faker->randomElement([Product::AVAILABLE_PRODUCT, Product::UNAVAILABLE_PRODUCT]),
		'image' => $faker->randomElement(['1.jpg','2.jpg', '3.jpg']),
		'seller_id' => User::all()->random()->id, //zwracanie id z randomowo wybranego ze wszystkich
	]
});

//TRANSACTION
$factory->define(Transaction::class, function (Faker $faker) {
	//Seller nie moze być Buyer
	//Losujemy randomowo usera ze wszystkich ktorzy maja produkty 
	$seller = Seller::has('products')->get()->random();
	//Losuemy randomowo uzytkownika ktory nie jest w/w Sellerem
	$buyer = User::all()->except($seller->id)->random();
	//Losujemy randomow produkt sellera
	$product = $seller->products->random();

	return [
		'quantity' => $faker->numberBetween(1, 3);
		'buyer_id' => $buyer->id;
		'product_id' => $product->id;
	]

});


