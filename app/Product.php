<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
use App\Seller;
use App\Transaction;

class Product extends Model
{
    const AVAILABLE_PRODUCT = 'available';
    const UNAVAILABLE_PRODUCT = 'unavailable';

    protected $fillable = [
    	'name',
    	'description',
    	'quantity',
    	'status',
    	'image',
    	'seller_id',
    ];

    /**
    * Check if Product is available
    * @return bool
    */
    public function isAvailable()
    {
    	return $this->status == Product::AVAILABLE_PRODUCT;
    }

    /**
    * Product belongs to one seller
    */
    public function seller()
    {
    	return $this->belongsTo(Seller::class);
    }

    /**
    * Product has many transactions
    */
    public function transactions()
    {
    	return $this->hasMany(Transaction::class);
    }

    /**
    * Product has many categories
    */
    public function categories()
    {
    	return $this->belongsToMany(Category::class);
    }
}
